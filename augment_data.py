import cv2
import os
	
# To check various image bounding box change image folder name
# there should be two folder one JPEGImages/ and labels/ 
img_dir = '/home/rohit/test/gun_detection/darknet_test2/darknet/JPEGImages'
label_dir = '/home/rohit/test/gun_detection/darknet_test2/darknet/labels'
# Funxtion for writing the label in text file 
def write_label(lab_list,path,lab):
	i=0
	with open(os.path.join(path,lab+"txt"),"a") as f:
		for k in lab_list:
			for num,i in enumerate(k):
				if num ==0:
					f.write(str(int(i)))
				else:
					f.write(" ")
					f.write(str(i))
					
			f.write('\n')

for img_path in os.listdir(img_dir):
	try:
		allboxes =  []
		allboxes2 = []
		allboxes3 = []
		img = cv2.imread(os.path.join(img_dir,img_path))
		img_orig = img
		#change the value of k[1] to augment the data 0-about x axis
		img = cv2.flip(img,-1)	
		h,w,c = img.shape
		b = os.path.join(img_dir[:-10],'labels',img_path[:-3]+'txt')
		print(img_path)
		f = open(b, "r").read().split('\n')
		for bbox in f:
			if len(bbox)>1:
				k = bbox.split(' ')
				k = list(map(float, k))
				k[1] = (1-k[1])
				k[2] = (1-k[2])
				k[3] = (k[3])
				k[4] = (k[4])
				allboxes.append(k)
		# for box in allboxes:
		# 	cv2.rectangle(img,(box[1]+(box[3]//2),box[2]+box[4]//2),(box[1]-(box[3]//2),box[2]-box[4]//2),(0,255,0),2)

		cv2.imwrite(os.path.join(img_dir,'both_axis_'+img_path),img)
		# Writing the label to file
	#	print(allboxes)
		write_label(allboxes,label_dir,'both_axis_'+img_path[:-3])

		# store values of k here
		cv2.imwrite(os.path.join(img_dir,'both_axis_blur_'+img_path),cv2.GaussianBlur(img,(9,9),0))
		# Writing the label to file
		write_label(allboxes,label_dir,'both_axis_blur_'+img_path[:-3])
		# store values of k here

	#	cv2.imshow("both_axis_",img)

		img2 = cv2.flip(img_orig,0)
		for bbox in f:
			if len(bbox)>1:
				l = bbox.split(' ')
				l = list(map(float, l))
				l[1] = (l[1])
				l[2] = (1-l[2])
				l[3] = (l[3])
				l[4] = (l[4])
				allboxes2.append(l)
		# for box in allboxes2:
		# 	cv2.rectangle(img2,(box[1]+(box[3]//2),box[2]+box[4]//2),(box[1]-(box[3]//2),box[2]-box[4]//2),(255,0,0),2)
		cv2.imwrite(os.path.join(img_dir,'y_axis_'+img_path),img2)
		# Writing the label to file
		write_label(allboxes2,label_dir,"y_axis_"+img_path[:-3])
		cv2.imwrite(os.path.join(img_dir,'y_axis_blur_'+img_path), cv2.GaussianBlur(img2,(9,9),0))
		# Writing the label to file
		write_label(allboxes2,label_dir,"y_axis_blur_"+img_path[:-3])
	#	cv2.imshow("y_axis_",img2)

		img3 = cv2.flip(img_orig,1)
		for bbox in f:
			if len(bbox)>1:
				m = bbox.split(' ')
				m = list(map(float, m))
				m[1] = (1-m[1])
				m[2] = (m[2])
				m[3] = (m[3])
				m[4] = (m[4])
				allboxes3.append(m)
		# for box in allboxes3:
		# 	cv2.rectangle(img3,(box[1]+(box[3]//2),box[2]+box[4]//2),(box[1]-(box[3]//2),box[2]-box[4]//2),(255,0,0),2)
		cv2.imwrite(os.path.join(img_dir,'x_axis_'+img_path),img3)
		# Writing the label to file
		write_label(allboxes3,label_dir,"x_axis_"+img_path[:-3])
		cv2.imwrite(os.path.join(img_dir,'x_axis_blur_'+img_path), cv2.GaussianBlur(img3,(9,9),0))
		# Writing the label to file
		write_label(allboxes3,label_dir,"x_axis_blur_"+img_path[:-3])
	#	cv2.imshow("x_axis_",img3)


		# press any ley except d and q twice to go to next image
		# press d to delete
		# if cv2.waitKey(0) & 0xFF == ord('d'):
		# 	os.remove(os.path.join(img_dir,img_path))
		# 	os.remove(b)
		# 	print('delete')
		# press q twice to quit

		if cv2.waitKey(1) & 0xFF == ord('q'):
			break
	except Exception as e:
		print('error in: ', img_path)
