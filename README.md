﻿﻿## Training YOLOv3 Object Detector

- Install awscli

`sudo pip3 install awscli` 

- Get the relevant OpenImages files needed to locate images of our interest

`wget https://storage.googleapis.com/openimages/2018_04/class-descriptions-boxable.csv`

`wget https://storage.googleapis.com/openimages/2018_04/train/train-annotations-bbox.csv`

- Download the training images and place in folders JPEGImages/ with labels in labels/ folder. You can custom label the images using https://github.com/tzutalin/labelImg

- Create the train-test split. Just add path of images in splitTrainAndTest.py

`python3 splitTrainAndTest.py` 

Give the correct path to the data JPEGImages folder. The 'labels' folder should be in the same directory as the JPEGImages folder.

- Install Darknet and compile it.
```
cd ~
git clone https://github.com/pjreddie/darknet
cd darknet
make
```
- Get the pretrained model

`wget https://pjreddie.com/media/files/darknet53.conv.74 -O ~/darknet/darknet53.conv.74`

- Fill in correct paths in the darknet.data file

- Start the training as below, by giving the correct paths to all the files being used as arguments. You can store the train log by creating a blank train.log file

`cd ~/darknet`

`./darknet detector train /path/to/darknet.data  /path/to/darknet-yolov3.cfg ./darknet53.conv.74 > /path/to/train.log`

- You can use `tail -f train.log | grep "avg"` to monitor losses. Keep on training till loss becomes 0.6.

## Test the code

- Give the correct path to the modelConfiguration and modelWeights files in object_detection_yolo.py and test any image or video for testing, e.g.

`python3 object_detection_yolo.py --image=gun.jpg`
`python3 object_detection_yolo.py --video=testVideo.mp4`

for more detailed instructions visit https://www.learnopencv.com/training-yolov3-deep-learning-based-custom-object-detector/